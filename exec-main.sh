#!/bin/bash

# Install VirtualBox if it is not already installed
VBOX_MANAGE_VERSION=`vboxmanage --version | grep 'command not found'`
if [[ -z "$VBOX_MANAGE_VERSION" ]]
then
  bash install-virtualbox.sh
fi

# Install Vagrant if it is not already installed
VAGRANT_VERSION=`vagrant --version | grep 'command not found'`
if [[ -z "$VAGRANT_VERSION" ]]
then
  bash install-vagrant.sh
fi

echo '-------------------------------------------'
echo 'sudo apt-get install -y python3 python3-pip'
sudo apt-get install -y python3 python3-pip

echo '----------------------'
echo 'pip3 install mechanize'
pip3 install mechanize

echo '---------------------'
echo 'pip3 install requests'
pip3 install requests

python3 install-packer.py

echo '--------------------'
echo 'VBoxManage --version'
VBoxManage --version

echo '-----------------'
echo 'vagrant --version'
vagrant --version

echo '----------------'
echo 'packer --version'
packer --version
