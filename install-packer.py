#!/usr/bin/env python3

import mechanize, requests, os

br = mechanize.Browser()
br.open('http://packer.io/downloads.html')
for link in br.links(url_regex='_linux_amd64.zip'):
  zip_file = requests.get(link.url) # Get link to *.zip file
  open('packer.zip', 'wb').write(zip_file.content) # Download file
  command = os.popen('unzip packer.zip; sudo mv packer /usr/local/bin')
  print(command.read())
  print(command.close())

